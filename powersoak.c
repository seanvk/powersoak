#define _GNU_SOURCE
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

/* compile time max */
#define MAX_FREQ 16
/* run time actual max */
int max_freq;

static int frequencies[MAX_FREQ];

#define MAX_LAT 16
int max_lat;

/* C state latency points for limiting deepest C state */
static int latency_points[MAX_LAT];

/* assumption is that all logical cpus allow the same frequencies */
static void
read_frequencies (void)
{
  FILE *file_maxfreq, *file_minfreq;
  char line_maxfreq[10], line_minfreq[10];
  int i, sc_max_freq, sc_min_freq, delta_freq;

  memset (line_maxfreq, 0, 10);
  file_maxfreq
      = fopen ("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq", "r");
  assert (file_maxfreq != NULL);

  fgets (line_maxfreq, 10, file_maxfreq);
  fclose (file_maxfreq);
  sc_max_freq = strtoull (line_maxfreq, NULL, 10);

  memset (line_minfreq, 0, 10);
  file_minfreq
      = fopen ("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq", "r");
  assert (file_minfreq != NULL);

  fgets (line_minfreq, 10, file_minfreq);
  fclose (file_minfreq);
  sc_min_freq = strtoull (line_minfreq, NULL, 10);

  printf ("%s%s%s", line_maxfreq, " ", line_minfreq);

  max_freq = 0;
  delta_freq = (sc_max_freq - sc_min_freq) / (MAX_FREQ - 1);
  for (i = 0; i < (MAX_FREQ - 1); i++)
    frequencies[max_freq++] = sc_min_freq + (delta_freq * i);
  printf ("Found %i frequencies\n", max_freq);
  assert (max_freq > 0);
}

/* assumption is that all logical cpus have the same C state exit latencies */

static void
read_latencies (void)
{
  FILE *file;
  char line[4096];
  DIR *dir;
  struct dirent *ent;

  dir = opendir ("/sys/devices/system/cpu/cpu0/cpuidle/");
  assert (dir != NULL);

  max_lat = 0;
  do
    {
      ent = readdir (dir);
      if (!ent)
        break;
      if (ent->d_name[0] == '.')
        continue;

      if (strstr (ent->d_name, "state") == NULL)
        continue;

      sprintf (line, "/sys/devices/system/cpu/cpu0/cpuidle/%s/latency",
               ent->d_name);
      file = fopen (line, "r");
      if (!file)
        continue;
      fgets (line, 4096, file);
      fclose (file);
      latency_points[max_lat++] = strtoull (line, NULL, 10);
    }
  while (ent);

  closedir (dir);

  printf ("Found %i C state latencies\n", max_lat);
  assert (max_lat > 0);
}

static void
random_sleep (int min_usec, int max_usec)
{
  int duration;
  int i;
  if (min_usec < max_usec)
    duration = rand () % (max_usec - min_usec) + min_usec;
  else
    duration = min_usec;

  usleep (min_usec);
  /* now spend at least a little CPU time */
  for (i = 0; i < 10000; i++)
    duration = duration + i;
}

static int
limit_cstate (int first_excluded_state)
{
  int maxlat = latency_points[first_excluded_state] - 1;
  int fd;

  fd = open ("/dev/cpu_dma_latency", O_WRONLY);
  if (fd >= 0)
    write (fd, &maxlat, 4);

  return fd;
}

static void
end_limit (int cookie)
{
  close (cookie);
}

static void *
thread (void *cookie)
{
  int cpu = (int)cookie;
  int mask = 1 << cpu;
  int start;
  char filename[4096];
  FILE *file;
  unsigned int rv = cpu + time (NULL);

  /* bind the thread to a specific CPU */
  sched_setaffinity (0, sizeof (mask), &mask);

  sprintf (filename, "/sys/devices/system/cpu/cpu%i/cpufreq/scaling_governor",
           cpu);
  file = fopen (filename, "w");
  assert (file != NULL);
  fprintf (file, "userspace\n");
  fclose (file);

  while (1)
    {
      int fd = 0;
      int freq;

      start = time (NULL);

      /* if cpu 0, pick a C state */
      if (cpu == 0)
        {
          int R;
          R = jrand48 (&rv) % max_lat;
          fd = limit_cstate (R);
        }

      /* pick a frequency to run at */
      freq = jrand48 (&rv) % max_freq;

      sprintf (filename,
               "/sys/devices/system/cpu/cpu%i/cpufreq/scaling_setspeed", cpu);
      file = fopen (filename, "w");
      if (file)
        {
          fprintf (file, "%i\n", frequencies[freq]);
          fclose (file);
        }

      /* one minute of random stuff */
      while (time (NULL) - start < 60)
        {
          random_sleep (500, 50000);
        }

      /* let the C state go */
      if (cpu == 0)
        end_limit (fd);
    }
  return NULL;
}

#define CPUCOUNT 4

int
main (int argc, char **argv)
{
  int i;
  pthread_t tid;

  srandom (time (NULL));
  read_frequencies ();
  read_latencies ();

  for (i = 0; i < CPUCOUNT; i++)
    {
      pthread_create (&tid, NULL, thread, (void *)i);
    }

  while (1)
    sleep (50000);
  return EXIT_SUCCESS;
}
