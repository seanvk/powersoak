all: powersoak

CFLAGS += -O2 -Wall -W 


OBJS := powersoak.o

powersoak: $(OBJS)
	gcc -o powersoak $(OBJS) -lpthread



%.o: %.c Makefile
	@echo "  CC  $<"
	@[ -x /usr/bin/cppcheck ] && /usr/bin/cppcheck -q $< || :
	@$(CC) $(CFLAGS) -c -o $@ $<


clean:
	rm  -f *~ powersoak *.o
	
